##    open-admin-pro(专业版)

---
### 简介
基于iview Admin 前端框架<a href="https://github.com/iview/iview-admin">传送门</a>  
<a target="_blank" href="http://pro.openc.top">演示地址</a>
 
#### 服务端代码仓库
##### 开源不易，请随手给个Star! 感谢支持！
<a target="_blank" href="https://gitee.com/liuyadu">码云</a>  <a target="_blank" href="https://github.com/liuyadu/">Github</a>  

#### 按钮权限控制
 全局函数
 $auth('功能编码,功能编码2') 多个用,号隔开  
 ```html
 <Button v-show="$auth('systemUserCreate')" type="primary">
 ```
#### 全局注册应用配置
  this.$config  // 配置参数  
  this.$http // 封装请求工具类,包含 baseURL  
  this.$apis  // API接口地址    
  this.$rules  // 表单规则  
  this.$auth  // 权限控制
 
#### Table表格插件
https://xuliangzhan.github.io/vxe-table/#/table/grid/reverse

#### 初始化安装
```bush
// install dependencies
npm install
```
#### 修改配置(/src/.env || /src/.env.preview)
```
  NODE_ENV=production
  VUE_APP_PREVIEW=false
  VUE_APP_API_BASE_URL=http://$apis.openc.top
```
#### 本地运行
```bush
npm run dev
```
#### 打包预览
```bush
npm run build:preview
```
#### 打包部署
```bush
npm run build
```
