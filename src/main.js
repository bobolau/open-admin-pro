// ie polyfill
import '@babel/polyfill'
import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import bootstrap from './libs/bootstrap'
import './assets/css/table.less'

// import './mock'

import './utils/filter'

// 组件引用
import './libs/use'

// eslint-disable-next-line no-new
new Vue({
  el: '#app',
  router,
  store,
  created: bootstrap,
  render: h => h(App)
})
