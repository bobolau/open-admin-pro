<template>
  <div class="search">
    <Form ref="form" :model="formItem" :rules="formItemRules" :label-width="100">
      <FormItem label="路由名称" prop="routeDesc">
        <Input v-model="formItem.routeDesc" placeholder="请输入内容"></Input>
      </FormItem>
      <FormItem label="路由编码" prop="routeName">
        <Input v-model="formItem.routeName" placeholder="默认使用服务名称{application.name}"></Input>
      </FormItem>
      <FormItem label="路由前缀" prop="path">
        <Input v-model="formItem.path" placeholder="/{path}/**"></Input>
      </FormItem>
      <FormItem label="路由方式">
        <Select v-model="selectType">
          <Option value="service" label="负载均衡(serviceId)"></Option>
          <Option value="url" label="反向代理(url)"></Option>
        </Select>
      </FormItem>
      <FormItem
        v-if="selectType==='service'"
        label="负载均衡"
        prop="serviceId"
        :rules="{required: true, message: '服务名称不能为空', trigger: 'blur'}">
        <Input v-model="formItem.serviceId" placeholder="服务名称{application.name}"></Input>
      </FormItem>
      <FormItem
        v-if="selectType==='url'"
        label="反向代理"
        prop="url"
        :rules="[{required: true, message: '服务地址不能为空', trigger: 'blur'},{type: 'url', message: '请输入有效网址', trigger: 'blur'}]">
        <Input v-model="formItem.url" placeholder="http://localhost:8080"></Input>
      </FormItem>
      <FormItem label="忽略前缀">
        <RadioGroup v-model="formItem.stripPrefix" type="button">
          <Radio :label="0">否</Radio>
          <Radio :label="1">是</Radio>
        </RadioGroup>
      </FormItem>
      <FormItem label="失败重试">
        <RadioGroup v-model="formItem.retryable" type="button">
          <Radio :label="0">否</Radio>
          <Radio :label="1">是</Radio>
        </RadioGroup>
      </FormItem>
      <FormItem label="状态">
        <RadioGroup v-model="formItem.status" type="button">
          <Radio :label="0">禁用</Radio>
          <Radio :label="1">正常</Radio>
        </RadioGroup>
      </FormItem>
    </Form>
  </div>
</template>

<script>
export default {
  name: 'ApiInfo',
  data () {
    return {
      isAdd: true,
      selectType: 'service',
      formItemRules: {
        routeDesc: [
          { required: true, message: '路由名称不能为空', trigger: 'blur' }
        ],
        routeName: [
          { required: true, message: '路由编码不能为空', trigger: 'blur' }
        ],
        path: [
          { required: true, message: '路由前缀不能为空', trigger: 'blur' }
        ]
      },
      formItem: this.getFormItem()
    }
  },
  methods: {
    getFormItem () {
      return {
        routeId: '',
        path: '',
        serviceId: '',
        url: '',
        stripPrefix: 0,
        retryable: 0,
        status: 1,
        routeName: '',
        routeDesc: ''
      }
    },
    setData (data) {
      this.isAdd = true
      if (data && data.routeId) {
        Object.assign(this.formItem, {
          routeId: data.routeId,
          path: data.path,
          serviceId: data.serviceId,
          url: data.url,
          stripPrefix: data.stripPrefix,
          retryable: data.retryable,
          status: data.status,
          routeName: data.routeName,
          routeDesc: data.routeDesc
        })
        this.isAdd = false
      }
      this.formItem.url = this.formItem.service ? '' : this.formItem.url
      this.formItem.serviceId = this.formItem.url ? '' : this.formItem.serviceId
      this.selectType = this.formItem.url ? 'url' : 'service'
    },
    handleReset () {
      this.$refs['form'].resetFields()
      this.formItem = this.getFormItem()
    },
    handleSubmit () {
      return new Promise((resolve, reject) => {
        this.$refs['form'].validate((valid) => {
          if (valid) {
            this.$http.post(this.$apis.routes.save, this.formItem, this)
              .then(res => {
                resolve(res)
              }).catch(function (err) {
                reject(err)
              })
          } else {
            resolve(false)
          }
        })
      })
    }
  },
  created () {

  },
  mounted () {
  }
}
</script>

<style scoped lang="less" rel="stylesheet/less">

</style>
